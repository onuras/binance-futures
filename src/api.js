import axios from 'axios';
import crypto from 'crypto';
import querystring from 'querystring';
import JSONBig from 'json-bigint';
import { ToastProgrammatic as Toast } from 'buefy';
import {
  SET_PRICE,
  SET_TICKER,
  ORDER_TRADE_UPDATE,
  ACCOUNT_UPDATE,
} from './store/mutation-types';

class API {
  socket = null;

  key = '';

  secret = '';

  testnet = false;

  listenKey = '';

  // axios instance
  instance = null;

  store = null;

  constructor(store, key, secret, testnet) {
    this.store = store;
    this.key = key;
    this.secret = secret;
    this.testnet = testnet;

    this.instance = axios.create({
      baseURL: testnet
        ? '/binance-futures-testnet-api'
        : '/binance-futures-api',
      headers: {
        'X-MBX-APIKEY': this.key,
      },
      transformResponse: (data) => JSONBig.parse(data),
    });

    this.instance.interceptors.response.use(
      (response) => response,
      (error) => {
        Toast.open({
          message: error.response.data?.msg || 'Unable to handle request',
          type: 'is-danger',
        });
        return Promise.reject(error);
      },
    );
  }

  connectSocket(listenKey) {
    this.listenKey = listenKey;

    if (this.socket) {
      this.socket.onopen = null;
      this.socket.onclose = null;
      this.socket.onmessage = null;
      this.socket.onerror = null;
    }

    this.socket = new WebSocket(
      this.testnet
        ? 'wss://stream.binancefuture.com/stream'
        : 'wss://fstream.binance.com/stream',
    );

    // socket events
    this.socket.onopen = (e) => this.onopen(e);
    // this.socket.onclose = e => this.onclose(e);
    this.socket.onmessage = (e) => this.onmessage(e);
    // this.socket.onerror = () => this.reconnect();
  }

  onopen() {
    this.subscribe();
  }

  onmessage(event) {
    const events = JSONBig.parse(event.data).data;
    if (Array.isArray(events)) {
      events.forEach((e) => this.processEvent(e));
    } else {
      this.processEvent(events);
    }
  }

  subscribe() {
    this.socket.send(JSON.stringify({
      method: 'SUBSCRIBE',
      params: [
        '!markPrice@arr',
        '!miniTicker@arr',
        this.listenKey,
      ],
      id: 1,
    }));
  }

  processEvent(event) {
    if (!event) {
      return;
    }

    if (event.e === 'markPriceUpdate') {
      this.store.commit(SET_PRICE, event);
    } else if (event.e === '24hrMiniTicker') {
      this.store.commit(SET_TICKER, event);
    } else if (event.e === 'ORDER_TRADE_UPDATE') {
      this.store.commit(ORDER_TRADE_UPDATE, this.convertOrder(event));
    } else if (event.e === 'ACCOUNT_UPDATE') {
      this.store.commit(ACCOUNT_UPDATE, event);
    } else {
      console.log(JSON.stringify(event));
    }
  }

  createSignature(params) {
    const timestamp = Date.now();
    const hmac = crypto.createHmac('sha256', this.secret);
    const qs = querystring.stringify(params);
    hmac.update(qs);
    return {
      timestamp,
      signature: hmac.digest('hex'),
    };
  }

  hmacParams(params) {
    Object.assign(params, { timestamp: Date.now() });
    const hmac = crypto.createHmac('sha256', this.secret);
    const qs = querystring.stringify(params);
    hmac.update(qs);
    Object.assign(params, { signature: hmac.digest('hex') });

    return params;
  }

  login() {
    return this.instance.post('/fapi/v1/listenKey');
  }

  extendListenKey() {
    return this.instance.put('/fapi/v1/listenKey');
  }

  getExchangeInfo() {
    return this.instance.get('/fapi/v1/exchangeInfo');
  }

  getAccountInfo() {
    return this.instance.request({
      url: '/fapi/v2/account',
      method: 'GET',
      params: this.hmacParams({}),
    });
  }

  getIncomeHistory() {
    return this.instance.request({
      url: '/fapi/v1/income',
      method: 'GET',
      params: this.hmacParams({}),
    });
  }

  getOrders(symbol) {
    return this.instance.request({
      url: '/fapi/v1/allOrders',
      method: 'GET',
      params: this.hmacParams({ symbol }),
    });
  }

  cancelOrder(symbol, orderId) {
    return this.instance.request({
      url: '/fapi/v1/order',
      method: 'DELETE',
      params: this.hmacParams({ symbol, orderId }),
    });
  }

  // TODO: This doesn't work due to some CORS errors
  // for some reason binance doesn't send CORS headers for this endpoint
  cancelAllOpenOrders(symbol) {
    return this.instance.request({
      url: '/fapi/v1/allOpenOrders',
      method: 'DELETE',
      params: this.hmacParams({ symbol }),
    });
  }

  order(order) {
    return this.instance.request({
      url: '/fapi/v1/order',
      method: 'POST',
      data: querystring.stringify(
        this.hmacParams(order),
      ),
    });
  }

  openPosition({
    symbol, entry, quantity, stop, target,
  }) {
    const orders = [
      // Buy/Sell order
      {
        symbol,
        side: quantity > 0 ? 'BUY' : 'SELL',
        type: 'LIMIT',
        quantity: Math.abs(quantity),
        price: Math.round(entry * 100) / 100,
        timeInForce: 'GTC',
      },
      // Stop order
      {
        symbol,
        side: quantity < 0 ? 'BUY' : 'SELL',
        type: 'STOP_MARKET',
        stopPrice: Math.round(stop * 100) / 100,
        closePosition: true,
      },
      // Take profit
      {
        symbol,
        side: quantity < 0 ? 'BUY' : 'SELL',
        type: 'STOP',
        // This is proper solution but it doesn't work if
        // entryprice equals current price:
        // stopPrice: Math.round(entry * 100) / 100,
        //
        // This is one solution but doesn't work for coins with smaller price (i.e LTC)
        // stopPrice: quantity < 0 ? Math.ceil(entry) : Math.floor(entry),
        stopPrice: quantity < 0
          ? Math.round((entry * 1.0001) * 100) / 100
          : Math.round((entry / 1.0001) * 100) / 100,
        quantity: Math.abs(quantity),
        price: Math.round(target * 100) / 100,
        reduceOnly: true,
      },
    ];

    // For some fucking reason batchOrders doesn't work
    // return this.instance.request({
    //   url: '/fapi/v1/batchOrders',
    //   method: 'POST',
    //   data: querystring.stringify(
    //     this.hmacParams({ batchOrders: encodeURIComponent(orders) }),
    //   ),
    // });

    // This is working but not the way I want
    // return Promise.all([
    //   this.order(orders[0]),
    //   this.order(orders[1]),
    //   this.order(orders[2]),
    // ]);

    // Cancel all orders first
    // Then try to create close order first
    this.cancelAllOpenOrders(symbol)
      .then(() => this.order(orders[2]))
      .then(() => Promise.all([
        // Then create all orders
        this.order(orders[0]),
        this.order(orders[1]),
      ]));
  }

  // order variable:
  //
  // {
  //   "e": "ORDER_TRADE_UPDATE",
  //   "T": 1613043138736,
  //   "E": 1613043138738,
  //   "o": {
  //     "s": "BTCUSDT",
  //     "c": "web_A8GPb2AcUM29TpD1A5OJ",
  //     "S": "BUY",
  //     "o": "LIMIT",
  //     "f": "GTC",
  //     "q": "0.236",
  //     "p": "42250",
  //     "ap": "0",
  //     "sp": "0",
  //     "x": "NEW",
  //     "X": "NEW",
  //     "i": 2632375191,
  //     "l": "0",
  //     "z": "0",
  //     "L": "0",
  //     "T": 1613043138736,
  //     "t": 0,
  //     "b": "9971",
  //     "a": "0",
  //     "m": false,
  //     "R": false,
  //     "wt": "CONTRACT_PRICE",
  //     "ot": "LIMIT",
  //     "ps": "BOTH",
  //     "cp": false,
  //     "rp": "0",
  //     "pP": false,
  //     "si": 0,
  //     "ss": 0
  //   }
  // }
  //
  //
  // This is how it looks like in orders state:
  //
  // {
  //   "orderId": 2632375191,
  //   "symbol": "BTCUSDT",
  //   "status": "NEW",
  //   "clientOrderId": "web_A8GPb2AcUM29TpD1A5OJ",
  //   "price": "42250",
  //   "avgPrice": "0.00000",
  //   "origQty": "0.236",
  //   "executedQty": "0",
  //   "cumQuote": "0",
  //   "timeInForce": "GTC",
  //   "type": "LIMIT",
  //   "reduceOnly": false,
  //   "closePosition": false,
  //   "side": "BUY",
  //   "positionSide": "BOTH",
  //   "stopPrice": "0",
  //   "workingType": "CONTRACT_PRICE",
  //   "priceProtect": false,
  //   "origType": "LIMIT",
  //   "time": 1613043138736,
  //   "updateTime": 1613043138736
  // }
  //
  // Example from website: https://binance-docs.github.io/apidocs/futures/en/#event-order-update
  // {
  //   "e":"ORDER_TRADE_UPDATE",     // Event Type
  //   "E":1568879465651,            // Event Time
  //   "T":1568879465650,            // Transaction Time
  //   "o":{
  //     "s":"BTCUSDT",              // Symbol
  //     "c":"TEST",                 // Client Order Id
  //       // special client order id:
  //       // starts with "autoclose-": liquidation order
  //       // "adl_autoclose": ADL auto close order
  //     "S":"SELL",                 // Side
  //     "o":"TRAILING_STOP_MARKET", // Order Type
  //     "f":"GTC",                  // Time in Force
  //     "q":"0.001",                // Original Quantity
  //     "p":"0",                    // Original Price
  //     "ap":"0",                   // Average Price
  //     "sp":"7103.04",             // Stop Price. Please ignore with TRAILING_STOP_MARKET order
  //     "x":"NEW",                  // Execution Type
  //     "X":"NEW",                  // Order Status
  //     "i":8886774,                // Order Id
  //     "l":"0",                    // Order Last Filled Quantity
  //     "z":"0",                    // Order Filled Accumulated Quantity
  //     "L":"0",                    // Last Filled Price
  //     "N":"USDT",             // Commission Asset, will not push if no commission
  //     "n":"0",                // Commission, will not push if no commission
  //     "T":1568879465651,          // Order Trade Time
  //     "t":0,                      // Trade Id
  //     "b":"0",                    // Bids Notional
  //     "a":"9.91",                 // Ask Notional
  //     "m":false,                  // Is this trade the maker side?
  //     "R":false,                  // Is this reduce only
  //     "wt":"CONTRACT_PRICE",      // Stop Price Working Type
  //     "ot":"TRAILING_STOP_MARKET",    // Original Order Type
  //     "ps":"LONG",                        // Position Side
  //     "cp":false,                     // If Close-All, pushed with conditional order
  //     "AP":"7476.89",             // Activation Price, only puhed with TRAILING_STOP_MARKET order
  //     "cr":"5.0",                 // Callback Rate, only puhed with TRAILING_STOP_MARKET order
  //     "rp":"0"                            // Realized Profit of the trade
  //   }
  // }
  convertOrder(order) {
    return {
      orderId: order.o.i,
      symbol: order.o.s,
      status: order.o.X,
      clientOrderId: order.o.c,
      price: order.o.p,
      avgPrice: order.o.ap,
      origQty: order.o.q,
      executedQty: 0, // FIXME: ???
      cumQuote: 0, // FIXME: ???
      timeInForce: order.o.f,
      type: order.o.o,
      reduceOnly: order.o.R,
      closePosition: order.o.cp,
      side: order.o.S,
      positionSide: order.o.ps,
      stopPrice: order.o.sp,
      workingType: order.o.wt,
      priceProtect: false, // FIXME: ???
      origType: order.o.ot,
      time: order.E,
      updateTime: order.T,
    };
  }
}

export default API;

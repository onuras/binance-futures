import Vue from 'vue';
import Vuex from 'vuex';
import API from '../api';
import {
  SET_API,
  SET_PRICE,
  SET_TICKER,
  SET_LOGGED_IN,
  SET_ACCOUNT_INFO,
  SET_ORDERS,
  ORDER_TRADE_UPDATE,
  ACCOUNT_UPDATE,
  SET_EXCHANGE_INFO,
  SET_SYMBOL,
  SET_HISTORY,
  SET_LOGIN_OPTIONS,
} from './mutation-types';

Vue.use(Vuex);

export default new Vuex.Store({
  // strict: process.env.NODE_ENV !== 'production',
  state: {
    price: {},
    ticker: {},
    symbol: 'ETHUSDT',
    loggedIn: false,
    api: null,
    account_info: {},
    orders: [],
    exchangeInfo: {},
    history: [],
    options: {
      key: null,
      secret: null,
      testnet: false,
    },
  },
  getters: {
    markPrice: (state) => (symbol) => state.price[symbol || state.symbol]?.p || 0,
    price: (state) => (symbol) => state.ticker[symbol || state.symbol]?.c || 0,
    availableBalance: (state) => state.account_info?.availableBalance || 0,
    // availableBalance: (state) => 100,
    symbols: (state) => Object.keys(state.ticker) || [],
    totalWalletBalance: (state) => state.account_info?.totalWalletBalance || 0,
    orders: (state) => (status, symbol) => state.orders.filter((o) => (
      o.symbol === (symbol || state.symbol) && o.status === status
    )),
    position: (state) => (symbol) => state.account_info?.positions?.find(
      (p) => p.symbol === (symbol || state.symbol),
    ),
    hasOpenPosition: (state, getters) => (symbol) => (
      getters.position(symbol || state.symbol) !== undefined
      && parseFloat(getters.position(symbol || state.symbol).positionAmt) !== 0
    ),
    entryPrice: (state) => (symbol) => state.account_info?.positions?.find(
      (p) => p.symbol === (symbol || state.symbol),
    )?.entryPrice || 0,
    upnl: (state, getters) => (symbol) => {
      const { positionAmt, entryPrice } = getters.position(symbol || state.symbol);
      if (positionAmt > 0) {
        return Math.abs(positionAmt) * getters.price(symbol || state.symbol)
               - Math.abs(positionAmt) * entryPrice;
      }
      return Math.abs(positionAmt) * entryPrice
             - Math.abs(positionAmt) * getters.price(symbol || state.symbol);
    },
    closeOrder: (state, getters) => (symbol) => getters.orders('NEW', symbol).find(
      (o) => o.type === 'TAKE_PROFIT' || o.type === 'STOP' || o.type === 'LIMIT',
    ),
    stopOrder: (state, getters) => (symbol) => getters.orders('NEW', symbol).find(
      (o) => o.type === 'STOP' || o.type === 'STOP_MARKET',
    ),
    symbol: (state) => state.symbol,
  },
  mutations: {
    [SET_PRICE](state, data) {
      Vue.set(state.price, data.s, data);
    },
    [SET_TICKER](state, data) {
      Vue.set(state.ticker, data.s, data);
    },
    [SET_API](state, api) {
      state.api = api;
    },
    [SET_LOGGED_IN](state, status) {
      state.loggedIn = status;
    },
    [SET_ACCOUNT_INFO](state, accountInfo) {
      state.account_info = accountInfo;
    },
    [SET_ORDERS](state, orders) {
      state.orders = orders;
    },
    [SET_EXCHANGE_INFO](state, exchangeInfo) {
      state.exchangeInfo = exchangeInfo;
    },
    [SET_HISTORY](state, history) {
      history.reverse();
      state.history = history;
    },
    [SET_LOGIN_OPTIONS](state, options) {
      state.options.key = options.key;
      state.options.secret = options.secret;
      state.options.testnet = options.testnet;
      localStorage.setItem('options', JSON.stringify(state.options));
    },
    [ORDER_TRADE_UPDATE](state, order) {
      const idx = state.orders.findIndex((o) => o.clientOrderId === order.clientOrderId);

      if (idx === -1) {
        state.orders.push(order);
      } else {
        Vue.set(state.orders, idx, order);
      }
    },
    // refs:
    // https://binance-docs.github.io/apidocs/futures/en/#event-balance-and-position-update
    // https://binance-docs.github.io/apidocs/futures/en/#account-information-v2-user_data
    [ACCOUNT_UPDATE](state, update) {
      // Update balances
      update.a.B.forEach((b) => {
        const idx = state.account_info.assets.findIndex((a) => a.asset === b.a);
        Vue.set(state.account_info.assets[idx], 'walletBalance', b.wb);
        Vue.set(state.account_info.assets[idx], 'crossWalletBalance', b.cw);
        if (b.a === 'USDT') {
          Vue.set(state.account_info, 'totalWalletBalance', b.wb);
          Vue.set(state.account_info, 'availableBalance', b.wb);
          Vue.set(state.account_info, 'totalCrossWalletBalance', b.cw);
        }
      });

      // Update positions
      update.a.P.forEach((p) => {
        const idx = state.account_info.positions.findIndex(
          (pp) => pp.symbol === p.s,
        );
        Vue.set(state.account_info.positions[idx], 'positionAmt', p.pa);
        Vue.set(state.account_info.positions[idx], 'entryPrice', p.ep);
        Vue.set(state.account_info.positions[idx], 'unrealizedProfit', p.up);
        if (p.mt === 'isoolated') {
          Vue.set(state.account_info.positions[idx], 'isolated', true);
        }
        Vue.set(state.account_info.positions[idx], 'isolatedWallet', p.iw);
        Vue.set(state.account_info.positions[idx], 'positionSide', p.ps);
      });
    },
    [SET_SYMBOL](state, symbol) {
      state.symbol = symbol;
    },
  },
  actions: {
    login(context, apiOptions) {
      const api = new API(
        this,
        apiOptions.key,
        apiOptions.secret,
        apiOptions.testnet,
      );
      return api.login().then((response) => {
        api.connectSocket(response.data.listenKey);
        context.commit(SET_API, api);
        context.commit(SET_LOGGED_IN, true);

        setInterval(() => {
          api.extendListenKey();
        }, 30 * 60 * 1000);
      });
    },
    getAccountInfo(context) {
      context.state.api.getAccountInfo().then((response) => {
        context.commit(SET_ACCOUNT_INFO, response.data);
      });
    },
    getOrders(context) {
      context.state.api.getOrders(context.state.symbol).then((response) => {
        context.commit(SET_ORDERS, response.data);
      });
    },
    getExchangeInfo(context) {
      context.state.api.getExchangeInfo(context.state.symbol).then((response) => {
        context.commit(SET_EXCHANGE_INFO, response.data);
      });
    },
    cancelAllOpenOrders: (context) => context.state.api.cancelAllOpenOrders(context.state.symbol),
    cancelOrder: (context, orderId) => context.state.api.cancelOrder(
      context.state.symbol,
      orderId,
    ),
    setSymbol(context, symbol) {
      context.commit(SET_SYMBOL, symbol);
      context.dispatch('getOrders');
    },
    getHistory(context) {
      context.state.api.getIncomeHistory().then((response) => {
        context.commit(SET_HISTORY, response.data);
      });
    },
    limitOrder(context, {
      symbol, side, price, quantity,
    }) {
      return context.state.api.order({
        symbol: symbol || context.state.symbol,
        type: 'LIMIT',
        side,
        price,
        quantity,
        timeInForce: 'GTC',
      });
    },
    openPosition(context, {
      symbol, entry, quantity, stop, target,
    }) {
      return context.state.api.openPosition({
        symbol: symbol || context.state.symbol, entry, quantity, stop, target,
      });
    },
    saveLoginOptions(context, loginOptions) {
      context.commit(SET_LOGIN_OPTIONS, loginOptions);
    },
    loadOptions(context) {
      const optionsString = localStorage.getItem('options');
      if (optionsString) {
        const options = JSON.parse(optionsString);
        context.commit(SET_LOGGED_IN, options);
        return Promise.resolve(options);
      }
      return Promise.reject();
    },
  },
});

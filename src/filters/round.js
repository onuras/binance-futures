import Vue from 'vue';

Vue.filter('round', (price) => (Math.round(price * 100) / 100).toFixed(2));

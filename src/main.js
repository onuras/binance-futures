import Vue from 'vue';
import Buefy from 'buefy';
import App from './App.vue';
import store from './store';
import './style/style.scss';
import './filters/round';

Vue.use(Buefy);

Vue.config.productionTip = false;

new Vue({
  store,
  render: (h) => h(App),
}).$mount('#app');

module.exports = {
  devServer: {
    proxy: {
      '^/binance-futures-testnet-api': {
        target: 'https://testnet.binancefuture.com',
        pathRewrite: { '^/binance-futures-testnet-api': '/' },
        changeOrigin: true,
      },
      '^/binance-futures-api': {
        target: 'https://fapi.binance.com',
        pathRewrite: { '^/binance-futures-api': '/' },
        changeOrigin: true,
      }
    }
  }
};

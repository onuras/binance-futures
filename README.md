# app

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn serve
```

### Compiles and minifies for production
```
yarn build
```

### Lints and fixes files
```
yarn lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).


### Running with docker

```sh
docker run -p 8080:8080 registry.gitlab.com/onuras/binance-futures:latest
```

`docker-compose.yml`:

```yaml
version: "3.9"

services:
  frontend:
    image: registry.gitlab.com/onuras/binance-futures:latest
    ports:
    - "8080:8080"
```

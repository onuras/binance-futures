FROM node:14
COPY . /app
RUN cd /app && yarn install && yarn lint && yarn build

FROM nginx:1.19-alpine
COPY --from=0 /app/dist /app
COPY nginx.conf /etc/nginx/conf.d/default.conf
